package com.vidya;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableScheduling
public class ConsumerAppApplication {
	
	private static final Logger log = LoggerFactory.getLogger(ConsumerAppApplication.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${producer_host:localhost}")
	private String host;
	
	@Value("${producet_port:8080}")
	private String port;
	
	@Scheduled(fixedRate = 5000)
	public void consume() {
        
        Greeting greeting = restTemplate.getForObject("http://"+host+":"+port+"/greeting?name=vidya", Greeting.class);
        log.info(greeting.toString());
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ConsumerAppApplication.class, args);
	}
}
